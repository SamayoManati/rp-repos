# Lieux
## La Vallée des Morts
La Vallée des Morts est une vallée cachée au sein des montagnes adj'kaanes, dans laquelle les adj'kaans enterrent les adj'jaïns et leur famille depuis très longtemps. Cet endroit est considéré par les adj'kaans comme un lieu sacré. Il est interdit de s'y battre, sauf pour protéger les tombeaux.
La vallée est composée d'une enfilade de plusieurs profonds canyons. Au bords desdits canyons, de monumentales portes sont gravées. Il s'agit des entrées des tombeaux, creusés dans la montagne. Une fois tous ses occupants enterrés, le tombeau est refermé, et ses lourdes portes sont scellées par un sortilège.
Il est imprudent de s'aventurer à profaner ces tombeaux : même si l'ont parvient à ouvrir les portes, et à échapper aux multiples pièges qui ont été tendus à l'intérieur (magiques ou non), on doit en sortant affronter la colère des adj'kaans. Les rares profanateurs à s'y aventurer ont pour la plupart finis torturés, somairement massacrés, réduits en esclavage pour construire le prochain tombeau, été enfermés vivants dans le tombeau qu'ils voulaient profaner...
L'accès même à la vallée est inerdit au commun des mortels. Pour avoir le droit d'y entrer, il faut y avoir été autorisé par les Gardiens des Morts, les adj'kaans qui gardent la vallée.

Les tombeaux qui composent la vallée sont creusés par des esclaves. La plupart d'entre eux meurent à la tâche, et bien souvent les survivants sont exécutés ou enfermés vivants dans le tombeau lorsqu'on le scelle, afin d'éviter qu'il n'existe des guides connaissant le tombeau et capables d'y mener des profanateurs.

Des légendes racontent que la vallée est hantée par les âmes des personnes y étant enterrées. N'ayant jamais pu étayer cette rumeur de preuves concrètes, je la considère comme invraisemblable.


## La Clef de Voute
Ainsi est nommé le mont le plus haut des montagnes adj'kaanes. Il s'agit d'un des sommets les plus haut du monde. La nuit, le spectacle est grandiose : la voute stellaire est splendide comme nul part ailleurs.
Il est risqué d'essayer de gravir ce sommet sans guide local.


## L'oasis de Saash'tânqa
Il s'agit ni plus ni moins de la plus grande oasis du désert adj'kaan. Il n'est pas rare que deux, voire trois tribus y établissent leur camp en même temps.
Elle abrite aussi une vieille tour, qui selon les savants et historiens adj'kaans, date d'environ -1200, peut-être du règne du roi Babar. Il est extraordinaire qu'elle ai pu être conservée jusque là.

