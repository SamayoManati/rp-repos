# Armes
## Tal&wuml;ar d'hast
Né du besoin de plus de portée lors du maniement du tal&wuml;ar, le tal&wuml;ar d'hast est une arme composée d'un manche en bois de 1m50 à 1m80 (principalement en fonction de la taille de son utilisateur) surmonté d'une lame de tal&wuml;ar. Cette configuration le rend d'un maniement plus aisé qu'un tal&wuml;ar simple, sans pour autant perdre de son efficacité. Cette arme est surtout utilisée par les non-guerriers, pour la défense.

Il existe une variante, elle utilisée par des guerriers émérites, comportant une lame à chaque extrémité du manche, pointant dans la direction opposée de l'autre.


## Couteau
Comme dans tous les peuple, les adj'kaans ont des couteaux. Les leurs sont très soignés, et tous différents. Ils occupent une place à part entière dans les costumes adj'kaans : chaque adj'kaan en cache un sous les plis du sien.


## Couteau gazelle
Il tire son nom de sa ressemblance avec une patte de gazelle. C'est un couteau multifonctions, capable de faire office aussi bien de couteau que de hachette ou de machette. Son poids important (environ 1 kilogramme, le poids moyen d'une épée) le rend très efficace en travail de taille. Il mesure environ 30 à 40 centimètres de long. L'espèce de trou en forme de cœur à la base de la lame permet d'éviter qu'un liquide coulant sur la lame n'atteigne le manche.

