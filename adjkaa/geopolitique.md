# Géopolitique
## Conflits
### Empire Cholula
Si on ne peut pas parler de vrai conflit, puisque la guerre n'a pas été déclarée, de fortes tensions existent entre les tribus adj'kaanes et les cholulas. Ces tensions proviennent d'il y a très longtemps, et sont alimentées par les attaques cholulas en territoire adj'kaan d'une part, et par les raids esclavagistes adj'kaans en territoire cholula d'autre part.

### Dynastie Xiaomi
Là encore, bien qu'il n'y ait pas de guerre ouverte entre Xiaomi et Adj'kaa, les tensions sont fortes. En cause, les pratiques esclavagistes de certaines tribus, qui font des raids dans les villages frontaliers pour récupérer des esclaves, et les attaques des navires de commerce par les pirates adj'kaans.

### Riennanie
La fondation des cités coloniales riennaniennes sur le sol adj'kaan a provoqué un intense sentiment d'invasion chez les adj'kaans. Les attaques de pirates adj'kaans contre les navires de commerce et militaires riennaniens ont provoquées un sentiment d'insécurité chez les riennaniens. En conséquence de quoi les riennaniens ne sont que très rarement les bienvenus en dehors des cités coloniales, et les adj'kaans subissent une ostracisation à l'intérieur de celles-ci. On ne compte plus les règlements de compte entre adj'kaans et riennaniens, et ce, bien que la pègre soit dirigée par les adj'kaans (ce qui renforce encore d'avantage le stéréotype riennanien selon lequel tous les adj'kaans sont des esclavagistes, des pirates et des malfrats).
La marine riennanienne essaye depuis longtemps d'éradiquer la piraterie en mer d'Ilya, mais sans succès.

