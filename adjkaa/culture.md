# Culture
## Duel sur l'honneur
Une affaire de justice peut se régler avec un duel. Si les deux parties acceptent, le duel est organisé, et se fera au premier sang.
Si le plaignant gagne ce duel, l'accusé reconnait sa culpabilité et se plie aux demandes du plaignant. Si l'accusé gagne, le plaignant retire sa plainte.
Ce procédé est considéré comme une méthode de règlement à l'amiable.
